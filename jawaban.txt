Tugas Berlatih SQL Join

--Soal 1a Mengambil Data Users--

mysql> SELECT id, name, email
          -> FROM users;
+----+----------+--------------+
| id    | name       | email           |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+

--Soal 1b Mengambil data items--

mysql> SELECT *
    -> FROM items
    -> WHERE price > 1000000;
+-------------+-------------------------+---------+-------+-------------+
| name        | description             	     | price  		| stock | 		category_id |
+-------------+-------------------------+---------+-------+-------------+
| Sumsang b50 | hape keren merk Sumsang | 4000000 		|   100 		|           1 |
| IMHO Watch  | jam tangan jujur        	     | 2000000 		|    10 		|           1 |
+-------------+-------------------------+---------+-------+-------------+

--Soal 1c Menampilkan data items join dengan kategori--
mysql> SELECT items.name, items.description, items.pricce, items.stock, items.category_id, categories.id, categories.name
    -> FROM items
    -> INNER JOIN categories
    -> ON items.category_id = categories.id;
+-------------+-------------------------+---------+-------+-------------+----+--------+
| name                | description             	     | price  	      | stock | category_id | id | name   |
+-------------+-------------------------+---------+-------+-------------+----+--------+
| Sumsang b50 | hape keren merk Sumsang  | 4000000 |   100 |                 1 |  1 | gadget   |
| Uniklooh         | baju keren brand        	      |  500000 |    50 |                   2 |  2 | cloth      |
| IMHO Watch   | jam tangan jujur                    | 2000000 |    10 |                  1 |  1 | gadget   |
+-------------+-------------------------+---------+-------+-------------+----+--------+

---Soal no.2 Mengubah Data dari Database---
mysql> UPDATE items
    -> SET prince = 2500000
    -> WHERE name = "Sumsang b50";